+++
title  = "Open Library Short-cut Demo"
author = ["Aron Gile"]
layout = "single"
draft  = false
+++

## Open Library Shortcode Example

{{<open-book>}}9780140328721{{</open-book>}}
