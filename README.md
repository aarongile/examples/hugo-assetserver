## Example [Hugo] website using Asset Servers

This project contains sample code to accompany this article:
[Integrating Asset Server with Hugo](https://simfish.dev/blog/integrate-asset-server-with-hugo). 


## Demo

 Check out the [demo site](https://aarongile.gitlab.io/examples/hugo-assetserver)

## Building locally

To work locally with this project, you'll have to follow
the steps below:

1. Install  Hugo(0.92+), Python3.6+(numpy, pandas, & matplotlib)
1. Fork, clone or download this project
1. Build the demo site using: `./build.sh`
1. Demo site can be accessed under: `localhost:1313`
