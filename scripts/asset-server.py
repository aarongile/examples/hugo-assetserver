from bottle import route, get, post, response, request, run

import os
import sys
import io
import requests
import re
from pathlib import Path

import matplotlib.pyplot as plt
import numpy  as np
import pandas as pd


# Open Library APIs
def openlib_authors(authors):

    result  = [ ]
    for author in authors:
        author_key  = author["key"]
        author_url  = 'https://openlibrary.org{0}.json'.format(author_key)
        response    = requests.get(author_url)
        author_json = response.json()
        result.append({
            "name":  author_json["name"],
            "link":  'https://openlibrary.org{0}'.format(author_key)
        })
    return result

def openlib_description( work ):
    work_id     = [ w["key"] for w in work["works"] ][0]
    detail_url  = 'https://openlibrary.org{0}.json'.format(work_id)
    response    = requests.get( detail_url)
    description = response.json()["description"]
    description = re.split(r'(?<=[.:;])\s', description)
    return ' '.join( description[:5] )

def openlib_work(isbn):
    'Returns details of a book given its ISBN'
    api_url  = 'https://openlibrary.org/isbn/{0}.json'.format(isbn)
    response = requests.get(api_url)
    work     = response.json()
    work_key = work["key"]
    links    = [
        'https://openlibrary.org{0}'.format( w["key"] ) for w in work["works"]
    ]
    return {
        "title":           work["title"],
        "publish_date":    work["publish_date"],
        "number_of_pages": work["number_of_pages"],
        "url":             'https://openlibrary.org{0}'.format(work["key"]),
        "cover_image":     'https://covers.openlibrary.org/b/isbn/{0}-M.jpg'.format(isbn),
        "authors":         openlib_authors(work["authors"]),
        "contributors":    work["contributions"],
        "publishers":      work["publishers"],
        "description":     openlib_description(work)
    }

@route('/openlib/books/<isbn>')
def openlib_book(isbn):

    work = openlib_work(isbn)
    return work

# time series plot
def series_plot(time, series, format="-", start=0, end=None):

    plt.plot(time[start:end], series[start:end], format)
    plt.xlabel('Time')
    plt.ylabel('Value')
    plt.grid(True)

@get('/time-series/plot/<city>')
def timeseries_plot(city):
    'Returns time-series plot given a list of city names'
    root_path = Path( '/opt/app/site/assets/time-series' )
    city_csv  = root_path.joinpath(city).resolve().absolute()

    df  = pd.read_csv(city_csv)
    df.drop(['D-J-F', 'M-A-M', 'J-J-A', 'S-O-N' ,'metANN'], axis=1, inplace=True)

    ts     = df.set_index('YEAR').stack()
    ts     = ts.replace(999.90, np.nan)
    series = ts.values
    time   = np.arange(ts.shape[0])
    plt.rcParams["figure.autolayout"] = True
    plt.rcParams["figure.figsize"] = [7.50, 3.50]
    #plt.figure(figsize=(10, 6))
    series_plot(time, series)


    plot_png = io.BytesIO()
    plt.savefig(plot_png, format='png')
    plot_png.seek(0)
    response.content_type="image/png"

    return plot_png


# Internal APs
@get('/server/test')
def server_test():
    'check if server has started'
    response.content_type = "text/plain; charset=utf-8"
    return u'OK! Server Is ONLINE!'

@get('/server/shutdown')
def server_shutdown():
    'Shutdown server'
    sys.stderr.close()
    response.content_type = "text/plain; charset=utf-8"
    return "! System Shutdown Completed"



# main
if __name__ == '__main__':

    port = int(os.environ.get('HUGO_ASSET_SERVER_PORT', 8080))
    run(host='0.0.0.0', port=port, debug=True)
