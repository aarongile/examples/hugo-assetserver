#! /bin/sh

build_env="$1"; shift;


# run demo
if [  "${build_env}" = "demo" ]; then
  echo "run: demo  ..."
  command -v hugo || {
     echo "no hugo binay found";
     exit 1
  }
  command -v python3 || {
    echo "no python3 binary found";
    exit 1;
  }

  cd ./scripts/
  python3 asset-server.py &
  sleep 3
  echo "testing asset server:"
  wget -qO- localhost:8080/server/test

  echo "starting hugo ..."
  cd ../site
  hugo serve

  echo "run: demo ..."
  exit 0
fi


# build ci/cd

echo "check hugo & python versions:"
hugo version
python3 --version
echo

echo  "container: files & directories"
pwd
ls -la
echo "/etc/hosts"
cat /etc/hosts
echo

echo "start bottle server ..."
cd  /opt/app/scripts 

#python3 -m pip install pandas numpy matplotlib
apk add  --no-cache py3-matplotlib py3-numpy py3-pandas

python3 asset-server.py &
cd /opt/app
echo "ping asset server ..."
# wait 5 seconds for bottle.py start
sleep 3
wget -O - localhost:8080/server/test
echo

echo "build site..."
cd /opt/app/site
# TODO: docker-compose .env file is not 
#       being sourced properly(why?)
# Use temporary hack for now
if [ -f "/opt/app/.env" ]; then
  . "/opt/app/.env" 
fi
echo "base url: ${SITE_BASE_URL}"
hugo -v -b "${SITE_BASE_URL}" 

echo "build result ..."

mkdir -p /opt/app/public
mv /opt/app/site/public/*  /opt/app/public || exit $?
ls -la /opt/app/public

wget -O - localhost:8080/server/shutdown
echo

exit 0
